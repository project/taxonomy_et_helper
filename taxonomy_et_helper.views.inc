<?php

/**
 * Implements hook_views_plugins()
 */
function taxonomy_et_helper_views_plugins(){
  return array(
    'argument validator' => array(
      'taxonomy_term_entity_translation' => array(
        'title' => t('Taxonomy term Entity Translation'),
        'handler' => 'TaxonomyTermEntityTranslationViewsArgumentValidator',
      ),
    ),
  );
}
