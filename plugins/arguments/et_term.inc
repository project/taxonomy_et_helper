<?php

/**
 * @file
 *
 * Plugin to provide an argument handler for a Taxonomy term
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Taxonomy term: ID [ET]"),
  // keyword to use for %substitution
  'keyword' => 'term',
  'description' => t('Creates a single taxonomy term from a taxonomy ID or taxonomy term name.'),
  'context' => 'ctools_et_term_context',
  'default' => array(
    'input_form' => 'tid',
    'language' => FALSE,
    'breadcrumb' => TRUE,
    'transform' => FALSE
  ),
  'settings form' => 'ctools_et_term_settings_form',
  'settings form validate' => 'ctools_et_term_settings_form_validate',
  'placeholder form' => 'ctools_et_term_ctools_argument_placeholder',
  'breadcrumb' => 'ctools_et_term_breadcrumb',
);

/**
 * Discover if this argument gives us the term we crave.
 */
function ctools_et_term_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('entity:taxonomy_term');
  }

  $vocabularies = $conf['vocabularies'];

  if (is_object($arg)) {
    $term = $arg;
  }
  else {
    switch ($conf['input_form']) {
      case 'tid':
      default:
        if (!is_numeric($arg)) {
          return FALSE;
        }
        $term = taxonomy_term_load($arg);
        break;

      case 'term':
        $transform = !empty($conf['transform']);
        $vocabularies = $conf['vocabularies'];

        $language = NULL;
        // Check if the language-context is set, otherwise avoid loading the 'language' context at all.
        if (!empty($conf['language'])) {
          $langContextPlugin = ctools_get_context('language');
          // @todo This check can be removed after language context merged into CTools.
          // @see issue https://www.drupal.org/node/2010896
          if (!empty($langContextPlugin)) {
            $languageContext = ctools_context_create('language', array());
            $language = $languageContext->data->language;
          }
        }

        $terms_et = taxonomy_et_helper_get_terms_from_translation($arg, $vocabularies, $transform, $language);
        // If no candidate terms have been found, then stop here.
        if (empty($terms_et)) {
          return FALSE;
        }

        // Loading the identified Terms from ET+Title.
        $query = new EntityFieldQuery();
        $query
          ->entityCondition('entity_type', 'taxonomy_term')
          ->entityCondition('entity_id', array_keys($terms_et), 'IN')
        ;

        $terms = $query->execute();
        // If no terms have been found, then stop here.
        if (!isset($terms['taxonomy_term']) || empty($terms['taxonomy_term'])) {
          return FALSE;
        }

        $term = reset($terms['taxonomy_term']);
        $term = taxonomy_term_load($term->tid);
        break;
    }

    if (empty($term)) {
      return NULL;
    }
  }

  if ($vocabularies && !isset($vocabularies[$term->vocabulary_machine_name])) {
    return NULL;
  }

  $context = ctools_context_create('entity:taxonomy_term', $term);
  $context->original_argument = $arg;
  return $context;
}

/**
 * Settings form for the argument
 */
function ctools_et_term_settings_form(&$form, &$form_state, $conf) {
  // @todo allow synonym use like Views does.
  $form['settings']['input_form'] = array(
    '#title' => t('Argument type'),
    '#type' => 'radios',
    '#options' => array('tid' => t('Term ID'), 'term' => t('Term name [ET+Title]')),
    '#default_value' => $conf['input_form'],
    '#prefix' => '<div class="clearfix">',
    '#suffix' => '</div>',
  );

  $languageContext = ctools_get_context('language');
  // @todo This check can be removed after language context merged into CTools.
  // @see issue https://www.drupal.org/node/2010896
  if (!empty($languageContext)) {
    $form['settings']['language'] = array(
      '#title' => t('Use the language context'),
      '#description' => t('If enabled, the term-name will be matched against the active language context, avoiding cross-language search of the TermID.'),
      '#type' => 'checkbox',
      '#default_value' => $conf['language'],
    );
  }
  else {
    $form['settings']['language'] = array(
      '#type' => 'hidden',
      '#value' => FALSE,
    );
  }

  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vid => $vocab) {
    $options[$vocab->machine_name] = $vocab->name;
  }

  $form['settings']['vocabularies'] = array(
    '#title' => t('Limit to these vocabularies'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => !empty($conf['vocabularies']) ? $conf['vocabularies'] : array(),
    '#description' => t('If no vocabularies are checked, terms from all vocabularies will be accepted.'),
  );

  $form['settings']['breadcrumb'] = array(
    '#title' => t('Inject hierarchy into breadcrumb trail'),
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['breadcrumb']),
    '#description' => t('If checked, taxonomy term parents will appear in the breadcrumb trail.'),
  );

  $form['settings']['transform'] = array(
    '#title' => t('Transform dashes in URL to spaces in term name filter values'),
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['transform']),
  );
//  return $form;
}

function ctools_et_term_settings_form_validate (&$form, &$form_state) {
  // Filter the selected vocabularies to avoid storing redundant data.
  $vocabularies = array_filter($form_state['values']['settings']['vocabularies']);
  form_set_value($form['settings']['vocabularies'], $vocabularies, $form_state);
}

/**
 * Form fragment to get an argument to convert a placeholder for preview.
 */
function ctools_et_term_ctools_argument_placeholder($conf) {
  switch ($conf['input_form']) {
    case 'tid':
    default:
      return array(
        '#type' => 'textfield',
        '#description' => t('Enter a taxonomy term ID.'),
      );
    case 'term':
      return array(
        '#type' => 'textfield',
        '#description' => t('Enter a taxonomy term name.'),
      );
  }
}

/**
 * Inject the breadcrumb trail if necessary.
 */
function ctools_et_term_breadcrumb($conf, $context) {
  if (empty($conf['breadcrumb']) || empty($context->data) || empty($context->data->tid)) {
    return;
  }

  $breadcrumb = array();
  $current = new stdClass();
  $current->tid = $context->data->tid;
  while ($parents = taxonomy_get_parents($current->tid)) {
    $current = array_shift($parents);
    $breadcrumb[] = l($current->name, 'taxonomy/term/' . $current->tid);
  }

  $breadcrumb = array_merge(drupal_get_breadcrumb(), array_reverse($breadcrumb));
  drupal_set_breadcrumb($breadcrumb);
}

